let form = document.querySelector("form");
form.addEventListener("submit", (event) => {
    event.preventDefault();

    let errorMsg = document.querySelector(".errorMsg");
    let message = document.querySelector("#message");
    let delivered = document.querySelector(".feedback");
    
    if (message.value === '') {
        errorMsg.style.display = "block";
        delivered.style.display = "none";

        setTimeout(() => {
            errorMsg.style.display = "none";
        }, 4000);
    } else {
        delivered.style.display = "block";
        delivered.innerHTML = message.value;
    }
});