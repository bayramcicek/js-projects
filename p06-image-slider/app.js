const pictures = ['1.png', '2.png', '3.png', '4.png', '5.png', '6.png'];

const imgContainer = document.querySelector(".img-container");
const buttons = document.querySelectorAll(".btn");

let counter = 0;

buttons.forEach((button) => {
    button.addEventListener("click", (event) => {
        if (button.classList.contains('btn-left')) {
            counter = (--counter) % pictures.length;
            imgContainer.style.backgroundImage = `url("./img/${pictures.at(counter)}")`;
        }

        if (button.classList.contains('btn-right')) {
            counter = (++counter) % pictures.length;
            imgContainer.style.backgroundImage = `url("./img/${pictures.at(counter)}")`;
        }
    });
});