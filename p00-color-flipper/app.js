const color = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F"];

const button = document.querySelector("#btn");
const hexText = document.getElementsByClassName('color')[0];

button.addEventListener('click', () => {
    let hexColor = '#';
    for (let i = 0; i < 6; i++) {
        hexColor += color[getRandomNumber()];
    }

    hexText.textContent = hexColor;
    document.body.style.backgroundColor = hexColor;
    
});

function getRandomNumber() {
    return Math.floor(Math.random() * color.length);
}