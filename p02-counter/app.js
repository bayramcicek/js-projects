let counter = 0;
let value = document.getElementById("value");
// console.log(value.textContent);
let buttons = document.querySelectorAll(".btn");

buttons.forEach(btn => {
    btn.addEventListener("click", (event) => {
        const buttonStyles = event.target.classList;
        if (buttonStyles.contains("decrease")) {
            counter--;
        } else if ((buttonStyles.contains("increase"))) {
            counter++;
        } else {
            counter = 0;
        }

        if (counter > 0) {
            value.style.color = "green";
        }
        else if (counter < 0) {
            value.style.color = "red";
        }
        else {
            value.style.color = "#222";
        }

        value.textContent = counter;
    });
});